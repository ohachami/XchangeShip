package com.coinship.api.front.dto;

public enum TraderTypeEnum {

	MAKER, TAKER;
}
