package com.coinship.api.front.validation;

import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;



	/**
	 * Implementation of {@link NotNullIfAnotherFieldHasValue} validator.
	 **/
public class ConditionalNullValueValidator
	    implements ConstraintValidator<ConditionalNullValue, Object> {

	    private String fieldName;
	    private String expectedFieldValue;
	    private String dependFieldName;

	    public void initialize(ConditionalNullValue annotation) {
	        fieldName          = annotation.fieldName();
	        expectedFieldValue = annotation.fieldValue();
	        dependFieldName    = annotation.dependFieldName();
	    }

	    public boolean isValid(Object value, ConstraintValidatorContext ctx) {

	        if (value == null) {
	            return true;
	        }

	        try {
	            String fieldValue       = BeanUtils.getProperty(value, fieldName);
	            String dependFieldValue = BeanUtils.getProperty(value, dependFieldName);

	            if (expectedFieldValue.equals(fieldValue) && dependFieldValue == null) {
	                ctx.disableDefaultConstraintViolation();
	                ctx.buildConstraintViolationWithTemplate(ctx.getDefaultConstraintMessageTemplate())
	                	.addPropertyNode(dependFieldName)
	                    .addConstraintViolation();
	                    return false;
	            }

	        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
	            throw new RuntimeException(ex);
	        }

	        return true;
	    }

	}
