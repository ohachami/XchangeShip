package com.coinship.api.front.dto;

public enum ShipOrderType {
	MARKET, LIMIT, STOP;
}
