package com.coinship.api.front.dto;

public enum SignEnum {

	DEBIT("D"), CREDIT("C");
	
	private String value;
	
	private SignEnum(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
}
