package com.coinship.api.front.dto;

import java.math.BigDecimal;

public final class ArithmeticOperation {
	public static final BigDecimal BIG_CENT = BigDecimal.valueOf(100L);
	public static final int EQUALS = 0;
	public static final int GREATER = 1;
	public static final int LESSER = -1;
	
	public static boolean greaterThan(BigDecimal biga, BigDecimal bigd) {
		return biga.compareTo(bigd) == GREATER;
	}
	
	public static boolean greaterOrEquals(BigDecimal biga, BigDecimal bigd) {
		return biga.compareTo(bigd) != LESSER;
	}
	
	public static boolean lesserThan(BigDecimal biga, BigDecimal bigd) {
		return biga.compareTo(bigd) == LESSER;
	}
	
	public static boolean lesserOrEquals(BigDecimal biga, BigDecimal bigd) {
		return biga.compareTo(bigd) != GREATER;
	}
	
	public static boolean between(BigDecimal biga, BigDecimal b1, BigDecimal b2) {
		return greaterOrEquals(biga, b1) && lesserOrEquals(biga, b2);
	}
}
