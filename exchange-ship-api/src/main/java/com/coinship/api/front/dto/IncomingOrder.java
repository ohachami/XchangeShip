package com.coinship.api.front.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.coinship.api.front.validation.ConditionalNullValue;

@XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
@XmlType(name = "incomingOrder", propOrder =  {
    "id",
    "code",
    "side",
    "price",
    "quantity",
    "orderType",
    "pair",
    "traderId"}
)
@ConditionalNullValue.List({@ConditionalNullValue(fieldName="orderType", fieldValue="LIMIT", dependFieldName="price"),
						    @ConditionalNullValue(fieldName="orderType", fieldValue="STOP", dependFieldName="price")})
public class IncomingOrder {

	
	private Integer id;
	private String code;
	@NotNull 
	private OrderSide side;
	private BigDecimal price;
	private BigDecimal limit;
	@NotNull @DecimalMin(value="0", inclusive=false)
	private BigDecimal quantity;
	@NotNull
	private String orderType;
	@NotNull
	private String pair;
	@NotNull
	private Integer traderId;
	
	/**
	 * @param side
	 * @param price
	 * @param quantity
	 * @param orderType
	 */
	public IncomingOrder(String code, OrderSide side, BigDecimal price, BigDecimal quantity,
			String orderType, String pair) {
		super();
		this.code = code;
		this.side = side;
		this.price = price;
		this.quantity = quantity;
		this.orderType = orderType;
	}


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}


	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * @return the side
	 */
	public OrderSide getSide() {
		return side;
	}


	/**
	 * @param side the side to set
	 */
	public void setSide(OrderSide side) {
		this.side = side;
	}


	/**
	 * @return the orderType
	 */
	public String getOrderType() {
		return orderType;
	}


	/**
	 * @param orderType the orderType to set
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IncomingOrder other = (IncomingOrder) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}


	/**
	 * @return the limit
	 */
	public BigDecimal getLimit() {
		return limit;
	}


	/**
	 * @param limit the limit to set
	 */
	public void setLimit(BigDecimal limit) {
		this.limit = limit;
	}


	/**
	 * @return the pair
	 */
	public String getPair() {
		return pair;
	}


	/**
	 * @param pair the pair to set
	 */
	public void setPair(String pair) {
		this.pair = pair;
	}
	
	@Override
	public String toString() {
		return code + " - " + price;
	}


	/**
	 * @return the traderId
	 */
	public Integer getTraderId() {
		return traderId;
	}


	/**
	 * @param traderId the traderId to set
	 */
	public void setTraderId(Integer traderId) {
		this.traderId = traderId;
	}


	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}


	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
}
