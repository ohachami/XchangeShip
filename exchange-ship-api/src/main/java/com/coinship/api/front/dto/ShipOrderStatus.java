package com.coinship.api.front.dto;

public enum ShipOrderStatus {
	POSTED, MATCHING, PENDING, COMPLETED, CANCELED ;
	
	public enum ShipFillStatus {
		NONE, PARTIALLY, COMPLETED;
	}
}
