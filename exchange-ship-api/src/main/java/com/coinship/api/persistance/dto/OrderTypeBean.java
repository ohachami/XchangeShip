package com.coinship.api.persistance.dto;

// Generated 15 nov. 2017 16:11:59 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;

/**
 * OrderType generated by hbm2java
 */
public class OrderTypeBean extends BasicDto implements java.io.Serializable {

	private int id;
	private String name;
	private Set<OrderPostBean> orderPosts = new HashSet<OrderPostBean>(0);

	public OrderTypeBean() {
	}

	public OrderTypeBean(int id) {
		this.id = id;
	}

	public OrderTypeBean(int id, String name, Set<OrderPostBean> orderPosts) {
		this.id = id;
		this.name = name;
		setCode(name);
		this.orderPosts = orderPosts;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		setCode(name);
		this.name = name;
	}

	public Set<OrderPostBean> getOrderPosts() {
		return this.orderPosts;
	}

	public void setOrderPosts(Set<OrderPostBean> orderPosts) {
		this.orderPosts = orderPosts;
	}

}
