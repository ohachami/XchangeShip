package com.coinship.api.persistance.dto;

public class TradingOperationBean {

	private AccountOperationBean makerSrcOperation;
	private AccountOperationBean makerDstOperation;
	
	private AccountOperationBean takerSrcOperation;
	private AccountOperationBean takerDstOperation;
	
	
	public TradingOperationBean() {
	}
	/**
	 * @param makerSrcOperation
	 * @param makerDstOperation
	 * @param takerSrcOperation
	 * @param takerDstOperation
	 */
	public TradingOperationBean(AccountOperationBean makerSrcOperation,
			AccountOperationBean makerDstOperation,
			AccountOperationBean takerSrcOperation,
			AccountOperationBean takerDstOperation) {
		super();
		this.makerSrcOperation = makerSrcOperation;
		this.makerDstOperation = makerDstOperation;
		this.takerSrcOperation = takerSrcOperation;
		this.takerDstOperation = takerDstOperation;
	}

	/**
	 * @return the makerSrcOperation
	 */
	public AccountOperationBean getMakerSrcOperation() {
		return makerSrcOperation;
	}
	/**
	 * @return the makerDstOperation
	 */
	public AccountOperationBean getMakerDstOperation() {
		return makerDstOperation;
	}
	/**
	 * @return the takerSrcOperation
	 */
	public AccountOperationBean getTakerSrcOperation() {
		return takerSrcOperation;
	}
	/**
	 * @return the takerDstOperation
	 */
	public AccountOperationBean getTakerDstOperation() {
		return takerDstOperation;
	}
	
	
}
