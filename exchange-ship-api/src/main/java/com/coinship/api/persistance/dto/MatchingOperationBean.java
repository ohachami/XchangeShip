package com.coinship.api.persistance.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MatchingOperationBean {

	private String pair;
	private String side;
	private BigDecimal quantity;
	private BigDecimal price;
	private Date transactionDate;
	
	public String getPair() {
		return pair;
	}
	public void setPair(String pair) {
		this.pair = pair;
	}
	public String getSide() {
		return side;
	}
	public MatchingOperationBean(String pair, String side, BigDecimal quantity, BigDecimal price,
			Date transactionDate) {
		super();
		this.pair = pair;
		this.side = side;
		this.quantity = quantity;
		this.price = price;
		this.transactionDate = transactionDate;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
}
