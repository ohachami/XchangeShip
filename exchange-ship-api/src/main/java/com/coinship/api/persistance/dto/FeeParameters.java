package com.coinship.api.persistance.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FeeParameters extends BasicDto implements Serializable{

	private int id;
	private BigDecimal volumeRate;
	private BigDecimal feeRate;
	private String applyTo;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the volumeRate
	 */
	public BigDecimal getVolumeRate() {
		return volumeRate;
	}
	/**
	 * @param volumeRate the volumeRate to set
	 */
	public void setVolumeRate(BigDecimal volumeRate) {
		this.volumeRate = volumeRate;
	}
	/**
	 * @return the feeRate
	 */
	public BigDecimal getFeeRate() {
		return feeRate;
	}
	/**
	 * @param feeRate the feeRate to set
	 */
	public void setFeeRate(BigDecimal feeRate) {
		this.feeRate = feeRate;
	}
	/**
	 * @return the applyTo
	 */
	public String getApplyTo() {
		return applyTo;
	}
	/**
	 * @param applyTo the applyTo to set
	 */
	public void setApplyTo(String applyTo) {
		this.applyTo = applyTo;
	}
	
}
