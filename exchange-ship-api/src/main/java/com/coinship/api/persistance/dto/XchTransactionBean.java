package com.coinship.api.persistance.dto;

// Generated 15 nov. 2017 16:11:59 by Hibernate Tools 4.0.0

import java.math.BigDecimal;
import java.util.Date;

/**
 * XchTransaction generated by hbm2java
 */
public class XchTransactionBean extends BasicDto implements java.io.Serializable {

	private int id;
	private OrderPostBean orderPost;
	private BigDecimal amount;
	private CurrencyBean currency;
	private String sign;
	private BigDecimal destUnitPrice;
	private Date transactionDate;
	private Boolean fee;
	private XchTransactionBean feeTrx;
	private AccountBean account;
	
	public XchTransactionBean() {
	}

	public XchTransactionBean(int id) {
		this.id = id;
	}

	public XchTransactionBean(int id, OrderPostBean orderPost, BigDecimal sourceAmt,
			BigDecimal destAmt, BigDecimal destUnitPrice, Date transactionDate) {
		this.id = id;
		this.orderPost = orderPost;
		this.amount = sourceAmt;
		this.destUnitPrice = destUnitPrice;
		this.transactionDate = transactionDate;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OrderPostBean getOrderPost() {
		return this.orderPost;
	}

	public void setOrderPost(OrderPostBean orderPost) {
		this.orderPost = orderPost;
	}

	public BigDecimal getDestUnitPrice() {
		return this.destUnitPrice;
	}

	public void setDestUnitPrice(BigDecimal destUnitPrice) {
		this.destUnitPrice = destUnitPrice;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public CurrencyBean getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(CurrencyBean currency) {
		this.currency = currency;
	}

	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @param sign the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * @return the fee
	 */
	public Boolean getFee() {
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(Boolean fee) {
		this.fee = fee;
	}
	
	public XchTransactionBean getFeeTrx() {
		return feeTrx;
	}

	public void setFeeTrx(XchTransactionBean feeTrx) {
		this.feeTrx = feeTrx;
	}

	public AccountBean getAccount() {
		return account;
	}

	public void setAccount(AccountBean account) {
		this.account = account;
	}

}
