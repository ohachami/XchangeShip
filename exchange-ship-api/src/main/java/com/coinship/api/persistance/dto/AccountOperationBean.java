package com.coinship.api.persistance.dto;

import com.coinship.api.front.dto.SignEnum;

public class AccountOperationBean {

	private SignEnum sign;
	private AccountBean account;
	private XchTransactionBean transaction;
	private XchTransactionBean fee;
	/**
	 * @return the sign
	 */
	public SignEnum getSign() {
		return sign;
	}
	/**
	 * @param sign the sign to set
	 */
	public void setSign(SignEnum sign) {
		this.sign = sign;
	}
	/**
	 * @return the transaction
	 */
	public XchTransactionBean getTransaction() {
		return transaction;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(XchTransactionBean transaction) {
		this.transaction = transaction;
	}
	/**
	 * @return the fee
	 */
	public XchTransactionBean getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(XchTransactionBean fee) {
		this.fee = fee;
	}
	/**
	 * @param sign
	 * @param account TODO
	 * @param transaction
	 * @param fee
	 * @param account
	 */
	public AccountOperationBean(SignEnum sign,
			AccountBean account, XchTransactionBean transaction, XchTransactionBean fee) {
		super();
		this.sign = sign;
		this.transaction = transaction;
		this.fee = fee;
		this.account = account;
	}
	public AccountBean getAccount() {
		return account;
	}
	public void setAccount(AccountBean account) {
		this.account = account;
	}
	
	
}
