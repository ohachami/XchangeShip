package com.coinship.orderbook;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.coinship.matcher.fees.FeeParamsManager;

public class FeeParamsTest {

	private FeeParamsManager feeParamsManager = new FeeParamsManager();
	
	@Test
	public void calculateMakerFeesTest() {
		feeParamsManager.setMakerFeesRate(new BigDecimal("0"));
		BigDecimal fee = feeParamsManager.calculateMakerFees(new BigDecimal("50000"));
		Assert.assertEquals(new BigDecimal("0"), fee);
	}
	
	
	@Test
	public void calculateTakerFeesTest() {
	
		feeParamsManager.setTakerFeesRate(new BigDecimal("0.24"));
		BigDecimal fee = feeParamsManager.calculateTakerFees(new BigDecimal("50000"));
		Assert.assertTrue(fee.compareTo(new BigDecimal("120")) == 0);
	}
}
