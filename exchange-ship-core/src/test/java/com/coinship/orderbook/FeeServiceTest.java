package com.coinship.orderbook;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.coinship.api.front.dto.SignEnum;
import com.coinship.api.front.dto.TraderTypeEnum;
import com.coinship.api.persistance.dto.CurrencyBean;
import com.coinship.api.persistance.dto.XchTransactionBean;
import com.coinship.matcher.fees.FeeParamsManager;
import com.coinship.matcher.fees.FeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/applicationContext-core-test.xml", "classpath*:/spring/applicationContext-db-test.xml" })
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class FeeServiceTest {

	@Autowired
	private FeeService feeService;
	
	@Test
	public void generateTakerFeeTransactionTest() {
		CurrencyBean cur = new CurrencyBean(1, "504", "MAD", null,null);
		FeeParamsManager feeParamsManager = new FeeParamsManager();
		feeParamsManager.setTakerFeesRate(new BigDecimal("0.24"));
		feeService.setFeeParamsManager(feeParamsManager);
		XchTransactionBean feeTrx = feeService.generateTransactionFees(TraderTypeEnum.TAKER, new BigDecimal("50000"), cur);
		Assert.assertTrue(feeTrx.getAmount().compareTo(new BigDecimal("120")) == 0);
		Assert.assertEquals(feeTrx.getCurrency(), cur);
		Assert.assertEquals(feeTrx.getSign(), SignEnum.DEBIT.getValue());
		
	}
	
	@Test
	public void generateMakerFeeTransactionTest() {
		CurrencyBean cur = new CurrencyBean(1, "999", "BTC", null,null);
		FeeParamsManager feeParamsManager = new FeeParamsManager();
		feeParamsManager.setMakerFeesRate(new BigDecimal("0.01"));
		feeService.setFeeParamsManager(feeParamsManager);
		XchTransactionBean feeTrx = feeService.generateTransactionFees(TraderTypeEnum.MAKER, new BigDecimal("0.493"), cur);
		Assert.assertTrue(feeTrx.getAmount().compareTo(new BigDecimal("0.0000493")) == 0);
		Assert.assertEquals(feeTrx.getCurrency(), cur);
		Assert.assertEquals(feeTrx.getSign(), SignEnum.DEBIT.getValue());
		
	}
}
