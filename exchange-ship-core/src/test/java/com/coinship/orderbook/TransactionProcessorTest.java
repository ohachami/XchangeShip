package com.coinship.orderbook;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.coinship.accounting.TransactionProcessor;
import com.coinship.api.front.dto.OrderSide;
import com.coinship.api.front.dto.SignEnum;
import com.coinship.api.persistance.dto.AccountBean;
import com.coinship.api.persistance.dto.CurrencyBean;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.api.persistance.dto.TradingOperationBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/applicationContext-core-test.xml" })
public class TransactionProcessorTest {

	@Autowired
	private TransactionProcessor transactionProcessor;
	private CurrencyBean src;
	private CurrencyBean dst;
	private PairCurrencyBean pair;
	private Map<CurrencyBean, AccountBean> inAccounts = new HashMap<CurrencyBean, AccountBean>();
	private Map<CurrencyBean, AccountBean> restAccounts = new HashMap<CurrencyBean, AccountBean>();
	
	@Before
	public void setUp() {
		src = new CurrencyBean(1, "999", "BTC",null, null);
		dst = new CurrencyBean(2, "504", "MAD",null, null);
		pair = new PairCurrencyBean(1, src, dst, "BTC_MAD", true, null);
		
		inAccounts.put(src, new AccountBean(1));
		inAccounts.put(dst, new AccountBean(2));
		
		restAccounts.put(src, new AccountBean(3));
		restAccounts.put(dst, new AccountBean(4));
		
		
	}
	
	@Test
	public void generateExchangeBuyerTestOk() {
		
		transactionProcessor.getFeeService().getFeeParamsManager().setTakerFeesRate(new BigDecimal("0.24"));
		transactionProcessor.getFeeService().getFeeParamsManager().setMakerFeesRate(new BigDecimal("0.02"));
		
		TradingOperationBean operation = transactionProcessor.getAccountingOperation(OrderSide.BUY, new BigDecimal("50000"), 
				new BigDecimal("0.492"), pair, inAccounts, restAccounts, null, null);
		
		//taker infos credit
		Assert.assertEquals(operation.getTakerSrcOperation().getAccount().getId(), 1);
		Assert.assertEquals(operation.getTakerSrcOperation().getSign(), SignEnum.CREDIT);
		Assert.assertEquals(operation.getTakerSrcOperation().getTransaction().getCurrency().getIsoCode(), "999");
		Assert.assertTrue(operation.getTakerSrcOperation().getTransaction().getAmount().compareTo(new BigDecimal("0.492")) == 0);
		//taker fees
		Assert.assertNull(operation.getTakerSrcOperation().getFee());
		
		//taker infos debit
		Assert.assertEquals(operation.getTakerDstOperation().getAccount().getId(), 2);
		Assert.assertEquals(operation.getTakerDstOperation().getSign(), SignEnum.DEBIT);
		Assert.assertEquals(operation.getTakerDstOperation().getTransaction().getCurrency().getIsoCode(), "504");
		Assert.assertTrue(operation.getTakerDstOperation().getTransaction().getAmount().compareTo(new BigDecimal("24600")) == 0);
		//taker fees
		Assert.assertEquals(operation.getTakerDstOperation().getFee().getCurrency().getCode(), "504");
		Assert.assertTrue(operation.getTakerDstOperation().getFee().getAmount().compareTo(new BigDecimal("59.04")) == 0);
		
		
		//maker infos credit
		Assert.assertEquals(operation.getMakerSrcOperation().getAccount().getId(), 3);
		Assert.assertEquals(operation.getMakerSrcOperation().getSign(), SignEnum.DEBIT);
		Assert.assertEquals(operation.getMakerSrcOperation().getTransaction().getCurrency().getIsoCode(), "999");
		Assert.assertTrue(operation.getMakerSrcOperation().getTransaction().getAmount().compareTo(new BigDecimal("0.492")) == 0);
		//maker fees
		Assert.assertEquals(operation.getMakerSrcOperation().getFee().getCurrency().getCode(), "999");
		Assert.assertTrue(operation.getMakerSrcOperation().getFee().getAmount().compareTo(new BigDecimal("0.0000984")) == 0);
		
		
		//maker infos debit
		Assert.assertEquals(operation.getMakerDstOperation().getAccount().getId(), 4);
		Assert.assertEquals(operation.getMakerDstOperation().getSign(), SignEnum.CREDIT);
		Assert.assertEquals(operation.getMakerDstOperation().getTransaction().getCurrency().getIsoCode(), "504");
		Assert.assertTrue(operation.getMakerDstOperation().getTransaction().getAmount().compareTo(new BigDecimal("24600")) == 0);
		//maker fees
		Assert.assertNull(operation.getMakerDstOperation().getFee());
	}
	
	
	@Test
	public void generateExchangeSellerTestOk() {
		
		transactionProcessor.getFeeService().getFeeParamsManager().setTakerFeesRate(new BigDecimal("0.24"));
		transactionProcessor.getFeeService().getFeeParamsManager().setMakerFeesRate(new BigDecimal("0.02"));
		
		TradingOperationBean operation = transactionProcessor.getAccountingOperation(OrderSide.SELL, new BigDecimal("50000"), 
				new BigDecimal("0.492"), pair, inAccounts, restAccounts, null, null);
		
		//maker infos credit
		Assert.assertEquals(operation.getMakerSrcOperation().getAccount().getId(), 3);
		Assert.assertEquals(operation.getMakerSrcOperation().getSign(), SignEnum.CREDIT);
		Assert.assertEquals(operation.getMakerSrcOperation().getTransaction().getCurrency().getIsoCode(), "999");
		Assert.assertTrue(operation.getMakerSrcOperation().getTransaction().getAmount().compareTo(new BigDecimal("0.492")) == 0);
		//maker fees
		Assert.assertNull(operation.getMakerSrcOperation().getFee());
		
		//maker infos debit
		Assert.assertEquals(operation.getMakerDstOperation().getAccount().getId(), 4);
		Assert.assertEquals(operation.getMakerDstOperation().getSign(), SignEnum.DEBIT);
		Assert.assertEquals(operation.getMakerDstOperation().getTransaction().getCurrency().getIsoCode(), "504");
		Assert.assertTrue(operation.getMakerDstOperation().getTransaction().getAmount().compareTo(new BigDecimal("24600")) == 0);
		//maker fees
		Assert.assertEquals(operation.getMakerDstOperation().getFee().getCurrency().getCode(), "504");
		Assert.assertTrue(operation.getMakerDstOperation().getFee().getAmount().compareTo(new BigDecimal("4.92")) == 0);
		
		
		//taker infos credit
		Assert.assertEquals(operation.getTakerSrcOperation().getAccount().getId(), 1);
		Assert.assertEquals(operation.getTakerSrcOperation().getSign(), SignEnum.DEBIT);
		Assert.assertEquals(operation.getTakerSrcOperation().getTransaction().getCurrency().getIsoCode(), "999");
		Assert.assertTrue(operation.getTakerSrcOperation().getTransaction().getAmount().compareTo(new BigDecimal("0.492")) == 0);
		//taker fees
		Assert.assertEquals(operation.getTakerSrcOperation().getFee().getCurrency().getCode(), "999");
		Assert.assertTrue(operation.getTakerSrcOperation().getFee().getAmount().compareTo(new BigDecimal("0.0011808")) == 0);
		
		
		//taker infos debit
		Assert.assertEquals(operation.getTakerDstOperation().getAccount().getId(), 2);
		Assert.assertEquals(operation.getTakerDstOperation().getSign(), SignEnum.CREDIT);
		Assert.assertEquals(operation.getTakerDstOperation().getTransaction().getCurrency().getIsoCode(), "504");
		Assert.assertTrue(operation.getTakerDstOperation().getTransaction().getAmount().compareTo(new BigDecimal("24600")) == 0);
		//taker fees
		Assert.assertNull(operation.getTakerDstOperation().getFee());
	}
}
