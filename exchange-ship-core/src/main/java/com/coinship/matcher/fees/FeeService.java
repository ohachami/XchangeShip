package com.coinship.matcher.fees;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coinship.api.front.dto.SignEnum;
import com.coinship.api.front.dto.TraderTypeEnum;
import com.coinship.api.persistance.dto.CurrencyBean;
import com.coinship.api.persistance.dto.XchTransactionBean;

@Service
public class FeeService {

	@Autowired
	private FeeParamsManager feeParamsManager;
	
	public XchTransactionBean generateTransactionFees(TraderTypeEnum traderType, BigDecimal transactionAmt, CurrencyBean cur) {
		if(transactionAmt != null && traderType != null) {
			XchTransactionBean feeTrx = new XchTransactionBean();
			feeTrx.setCurrency(cur);
			feeTrx.setAmount(TraderTypeEnum.TAKER.equals(traderType) ? feeParamsManager.calculateTakerFees(transactionAmt): 
				feeParamsManager.calculateMakerFees(transactionAmt));
			feeTrx.setSign(SignEnum.DEBIT.getValue());
			feeTrx.setTransactionDate(new Date());
			feeTrx.setFee(true);
			return feeTrx;
		}
		return null;
	}

	/**
	 * @param feeParamsManager the feeParamsManager to set
	 */
	public void setFeeParamsManager(FeeParamsManager feeParamsManager) {
		this.feeParamsManager = feeParamsManager;
	}

	/**
	 * @return the feeParamsManager
	 */
	public FeeParamsManager getFeeParamsManager() {
		return feeParamsManager;
	}
	
	
	
	
}
