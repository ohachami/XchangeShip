package com.coinship.matcher.fees;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.coinship.api.front.dto.ArithmeticOperation;

@Service
public class FeeParamsManager {

	private BigDecimal makerFeesRate;
	private BigDecimal takerFeesRate;
	
	/**
	 * @return the makerFeesRate
	 */
	public BigDecimal getMakerFeesRate() {
		return makerFeesRate;
	}
	/**
	 * @param makerFeesRate the makerFeesRate to set
	 */
	public void setMakerFeesRate(BigDecimal makerFeesRate) {
		this.makerFeesRate = makerFeesRate;
	}
	/**
	 * @return the takerFeesRate
	 */
	public BigDecimal getTakerFeesRate() {
		return takerFeesRate;
	}
	/**
	 * @param takerFeesRate the takerFeesRate to set
	 */
	public void setTakerFeesRate(BigDecimal takerFeesRate) {
		this.takerFeesRate = takerFeesRate;
	}
	
	public BigDecimal calculateMakerFees(BigDecimal amount) {
		return calculateFees(amount, makerFeesRate);
	}
	
	public BigDecimal calculateTakerFees(BigDecimal amount) {
		return calculateFees(amount, takerFeesRate);
	}
	
	private BigDecimal calculateFees(BigDecimal amount, BigDecimal rate) {
		if(amount != null) {
			return amount.multiply(rate).divide(ArithmeticOperation.BIG_CENT);
		}
		return BigDecimal.ZERO;
	}
	
	
}
