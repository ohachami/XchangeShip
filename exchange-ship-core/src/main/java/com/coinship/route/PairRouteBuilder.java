package com.coinship.route;

import java.util.List;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.orderbook.impl.OrderBookCache;
import com.coinship.service.ICurrencyPairService;

public class PairRouteBuilder extends RouteBuilder{

	@Autowired
	private ICurrencyPairService currencyPairService;
	private OrderBookCache orderBookCache;
	
	
	@Override
	public void configure() throws Exception {
		List<PairCurrencyBean> activePairs = currencyPairService.getActivePairs();
		if(activePairs != null) {
			
			for(PairCurrencyBean current : activePairs) {
				//create a queue for each active pair
				from(String.format("activemq:queue:%sQueue", current.getLabel()))
				.process(orderBookCache.getOrderBookFor(current))
				.log(LoggingLevel.INFO, "Order posted to matching engine successfully");
				
			}
		}
	}


	/**
	 * @return the currencyPairService
	 */
	public ICurrencyPairService getCurrencyPairService() {
		return currencyPairService;
	}


	/**
	 * @param currencyPairService the currencyPairService to set
	 */
	public void setCurrencyPairService(ICurrencyPairService currencyPairService) {
		this.currencyPairService = currencyPairService;
	}


	/**
	 * @return the orderBookCache
	 */
	public OrderBookCache getOrderBookCache() {
		return orderBookCache;
	}


	/**
	 * @param orderBookCache the orderBookCache to set
	 */
	public void setOrderBookCache(OrderBookCache orderBookCache) {
		this.orderBookCache = orderBookCache;
	}

}
