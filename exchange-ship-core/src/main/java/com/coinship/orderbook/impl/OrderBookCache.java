package com.coinship.orderbook.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.matcher.core.OrderBookListener;
import com.coinship.matcher.exception.OrderBookException;
import com.coinship.orderbook.IOrderBookCache;
import com.coinship.service.ICurrencyPairService;

@Service
public class OrderBookCache implements IOrderBookCache{

	@Autowired
	private ICurrencyPairService currencyPairService;
	
	private Map<PairCurrencyBean, OrderBookManager> orderBooks;
	
	@Autowired
	private OrderBookListener orderBookListener;
	
	public void init() throws OrderBookException {
		orderBooks = new HashMap<>();
		for(PairCurrencyBean current : currencyPairService.getActivePairs()) {
			orderBooks.put(current, new OrderBookManager(orderBookListener, current));
		}
	}
	
	public OrderBookManager getOrderBookFor(PairCurrencyBean pair) {
		return orderBooks.get(pair);
	}
}
