package com.coinship.orderbook.impl;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.matcher.core.OrderBook;
import com.coinship.matcher.core.OrderBookListener;
import com.coinship.matcher.exception.OrderBookException;

/**
 * This class create an orderbook for a given pair, then
 * cache the orderbook for better accessing performance 
 * @author Omar HACHAMI
 *
 */
public class OrderBookManager implements Processor{

	
	private OrderBook book;
	
	
	public OrderBookManager(OrderBookListener orderBookListener, PairCurrencyBean pair) throws OrderBookException {
		if(pair != null && pair.getLabel() != null) {
			book = new OrderBook(orderBookListener, pair);
		}else {
			throw new OrderBookException("Invalid pair", pair);
		}
		 
	}
	
	/**
	 * post an order to the orderbook
	 */
	@Override
	public void process(Exchange ex) throws Exception {
		IncomingOrder entryOrder = ex.getIn(IncomingOrder.class);
		book.enter(entryOrder);
	}
	
	/**
	 * Stop accepting more orders
	 * while fulfilling the in progress orders
	 * @param pair
	 */
	public void closeOrderBook(PairCurrencyBean pair) {
		//TODO shutting down gracefully the order book of the given pair
	}
	
	

}
