package com.coinship.accounting;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coinship.api.front.dto.OrderSide;
import com.coinship.api.front.dto.SignEnum;
import com.coinship.api.front.dto.TraderTypeEnum;
import com.coinship.api.persistance.dto.AccountBean;
import com.coinship.api.persistance.dto.AccountOperationBean;
import com.coinship.api.persistance.dto.CurrencyBean;
import com.coinship.api.persistance.dto.OrderPostBean;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.api.persistance.dto.TradingOperationBean;
import com.coinship.api.persistance.dto.XchTransactionBean;
import com.coinship.matcher.fees.FeeService;

/**
 * Handles accounting operations.
 * 
 * @author Omar HACHAMI
 *
 */
@Service
public class TransactionProcessor {

	@Autowired
	private FeeService feeService;
	
	public TradingOperationBean getAccountingOperation(OrderSide orderSide, BigDecimal price, BigDecimal amountSrc,
			PairCurrencyBean pair, Map<CurrencyBean, AccountBean> inAccounts,
			Map<CurrencyBean, AccountBean> restingAccount, Integer inOrderId, Integer restingOrderId) {

		AccountOperationBean makerSrcOperation = null;
		AccountOperationBean makerDstOperation = null;
		AccountOperationBean takerSrcOperation = null;
		AccountOperationBean takerDstOperation = null;
		BigDecimal amountDst = (BigDecimal)price.multiply(amountSrc);
		if(OrderSide.BUY.equals(orderSide)) {//the buyer is the taker
			//generate operations for the taker
			takerDstOperation = generateDebitOperation(inAccounts.get(pair.getCurrencyByDestCur()), amountDst, price, 
					TraderTypeEnum.TAKER, pair.getCurrencyByDestCur(), null);
			
			takerSrcOperation = generateCreditOperation(inAccounts.get(pair.getCurrencyBySourceCur()), amountSrc, 
					price, pair.getCurrencyBySourceCur(), null);
			
			//generate operation for the maker
			makerSrcOperation = generateDebitOperation(restingAccount.get(pair.getCurrencyBySourceCur()), amountSrc, price, 
					TraderTypeEnum.MAKER, pair.getCurrencyBySourceCur(), null);
			
			makerDstOperation = generateCreditOperation(restingAccount.get(pair.getCurrencyByDestCur()), amountDst, 
					price, pair.getCurrencyByDestCur(), null);
			
		}else if(OrderSide.SELL.equals(orderSide)) {//the seller is the taker
			
			//generate operations for the taker
			takerSrcOperation = generateDebitOperation(inAccounts.get(pair.getCurrencyBySourceCur()), amountSrc, price, 
					TraderTypeEnum.TAKER, pair.getCurrencyBySourceCur(), null);
			
			takerDstOperation = generateCreditOperation(inAccounts.get(pair.getCurrencyByDestCur()), amountDst, 
					price, pair.getCurrencyByDestCur(), null);
			
			//generate operation for the maker
			makerDstOperation = generateDebitOperation(restingAccount.get(pair.getCurrencyByDestCur()), amountDst, price, 
					TraderTypeEnum.MAKER, pair.getCurrencyByDestCur(), null);
			
			makerSrcOperation = generateCreditOperation(restingAccount.get(pair.getCurrencyBySourceCur()), amountSrc, 
					price, pair.getCurrencyBySourceCur(), null);
		}
		
		return new TradingOperationBean(makerSrcOperation, makerDstOperation, takerSrcOperation, takerDstOperation);
	}
	
	
	private AccountOperationBean generateDebitOperation(AccountBean sellerSrc, BigDecimal amount, BigDecimal price, TraderTypeEnum traderType, CurrencyBean cur, Integer orderId) {
		OrderPostBean order = new OrderPostBean(orderId);
		XchTransactionBean feeTrx = feeService.generateTransactionFees(traderType, amount, cur);
		feeTrx.setOrderPost(order);
		feeTrx.setAccount(sellerSrc);
		XchTransactionBean debitTrx = buildTransaction(SignEnum.DEBIT, amount, price, cur);
		debitTrx.setFeeTrx(feeTrx);
		debitTrx.setOrderPost(order);
		debitTrx.setAccount(sellerSrc);
		AccountOperationBean operation = new AccountOperationBean(SignEnum.DEBIT, sellerSrc, debitTrx, feeTrx);
		
		return operation;
	}
	
	private AccountOperationBean generateCreditOperation(AccountBean sellerDst, BigDecimal amount, BigDecimal price, CurrencyBean cur, Integer orderId) {
		OrderPostBean order = new OrderPostBean(orderId);
		XchTransactionBean creditTrx = buildTransaction(SignEnum.CREDIT, amount, price, cur);
		creditTrx.setOrderPost(order);
		creditTrx.setAccount(sellerDst);
		AccountOperationBean operation = new AccountOperationBean(SignEnum.CREDIT, sellerDst, creditTrx, null);
		
		return operation;
	}
	
	
	private XchTransactionBean buildTransaction(SignEnum sign, BigDecimal amount, BigDecimal price, CurrencyBean cur) {
			XchTransactionBean feeTrx = new XchTransactionBean();
			feeTrx.setCurrency(cur);
			feeTrx.setAmount(amount);
			feeTrx.setDestUnitPrice(price);
			feeTrx.setSign(sign.getValue());
			feeTrx.setTransactionDate(new Date());
			feeTrx.setFee(false);
			return feeTrx;
	}


	/**
	 * @return the feeService
	 */
	public FeeService getFeeService() {
		return feeService;
	}
	
	
	
}
