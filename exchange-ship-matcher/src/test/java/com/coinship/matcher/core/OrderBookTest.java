package com.coinship.matcher.core;

import java.math.BigDecimal;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.api.front.dto.OrderSide;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.matcher.exception.OrderBookException;

public class OrderBookTest {

	private OrderBook orderBook;
	
	@Before
	public void setUp() throws OrderBookException {
		orderBook = new OrderBook(EasyMock.mock(OrderBookListener.class), new PairCurrencyBean(1,
				null, null, "BTC-ETH", true, null));
		
		orderBook.enter(new IncomingOrder("0001", OrderSide.SELL, new BigDecimal(100), new BigDecimal(5), "LIMIT", "BTC-ETH"));
		orderBook.enter(new IncomingOrder("0002", OrderSide.SELL, new BigDecimal(99), new BigDecimal(5), "LIMIT", "BTC-ETH"));
		orderBook.enter(new IncomingOrder("0003", OrderSide.SELL, new BigDecimal(120), new BigDecimal(5), "LIMIT", "BTC-ETH"));
		orderBook.enter(new IncomingOrder("0004", OrderSide.SELL, new BigDecimal("98.99995"), new BigDecimal(5), "LIMIT", "BTC-ETH"));
	}
	
	
	@Test
	public void askOrderBestPriceOK() {
		System.out.println("Best price " + orderBook.getBestAsksLevel().getPrice());
		Assert.assertTrue("Expected best price : " + new BigDecimal("98.99995"), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal("98.99995")));
	}
	
	@Test
	public void matchLimitOrder_partial1_OK() throws OrderBookException {
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		Assert.assertTrue("Expected best price : " + new BigDecimal("98.99995"), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal("98.99995")));
		
		orderBook.enter(new IncomingOrder("0005", OrderSide.BUY, new BigDecimal(100), new BigDecimal(7), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(99), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(99)));
		
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0006", OrderSide.BUY, new BigDecimal(100), new BigDecimal(2), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(99), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(99)));
		
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0007", OrderSide.BUY, new BigDecimal(100), new BigDecimal(3), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(100), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(100)));
		
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0008", OrderSide.BUY, new BigDecimal(100), new BigDecimal(5), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(120), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(120)));
		
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0009", OrderSide.SELL, new BigDecimal(100), new BigDecimal(3), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(100), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(100)));
		
		System.out.println("MatchLimitOrder_partial1 Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0012", OrderSide.BUY, new BigDecimal(100), new BigDecimal(5), "LIMIT", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(120), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(120)));
	}
	
	@Test
	public void matchLimitOrder_partial2_OK() throws OrderBookException {
		System.out.println("matchLimitOrder_partial2_OK Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0009", OrderSide.BUY, null, new BigDecimal(13), "MARKET", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(100), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(100)));
		
		System.out.println("matchLimitOrder_partial2_OK Best price is " + orderBook.getBestAsksLevel().getPrice());
		orderBook.enter(new IncomingOrder("0010", OrderSide.BUY, null, new BigDecimal(2), "MARKET", "BTC-ETH"));
		Assert.assertTrue("Expected best price : " + new BigDecimal(120), orderBook.getBestAsksLevel().getPrice().equals(new BigDecimal(120)));
		
		orderBook.enter(new IncomingOrder("0011", OrderSide.BUY, null, new BigDecimal(7), "MARKET", "BTC-ETH"));
		Assert.assertNull("No offers", orderBook.getBestAsksLevel());
			
	}
}
