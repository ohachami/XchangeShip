package com.coinship.matcher.core;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.coinship.api.front.dto.ArithmeticOperation;


public class ArithmeticOperationTest {

	@Test
	public void lessThan_ok() {
		BigDecimal a1 = new BigDecimal("5");
		BigDecimal a2 = new BigDecimal("6");
		Assert.assertTrue(a1.toString() + " is not lesser than " + a2.toString(), ArithmeticOperation.lesserThan(a1, a2));
	}
	
	@Test
	public void lessThan_ko_1() {
		BigDecimal a1 = new BigDecimal("5");
		BigDecimal a2 = new BigDecimal("5");
		Assert.assertFalse(a1.toString() + " is lesser than " + a2.toString(), ArithmeticOperation.lesserThan(a1, a2));
	}
	
	@Test
	public void lessThan_ko_2() {
		BigDecimal a1 = new BigDecimal("5");
		BigDecimal a2 = new BigDecimal("4");
		Assert.assertFalse(a1.toString() + " is lesser than " + a2.toString(), ArithmeticOperation.lesserThan(a1, a2));
	}
	
	@Test
	public void between_ok() {
		BigDecimal a2 = new BigDecimal("4.99999999");
		BigDecimal a1 = new BigDecimal("5");
		BigDecimal a3 = new BigDecimal("5.00000001");
		Assert.assertTrue(a1.toString() + " is not between " + a2.toString() + " and " +a3.toString(),	ArithmeticOperation.between(a1, a2, a3));
	}
	
	@Test
	public void between_ko() {
		BigDecimal a2 = new BigDecimal("5.345");
		BigDecimal a1 = new BigDecimal("5");
		BigDecimal a3 = new BigDecimal("6");
		Assert.assertFalse(a1.toString() + " is not between " + a2.toString() + " and " +a3.toString(),	ArithmeticOperation.between(a1, a2, a3));
	}
}
