package com.coinship.matcher.core;

public enum OrderType {
	MARKET, LIMIT;
}
