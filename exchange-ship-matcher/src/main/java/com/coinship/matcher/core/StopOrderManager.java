package com.coinship.matcher.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import com.coinship.api.front.dto.ArithmeticOperation;
import com.coinship.api.front.dto.IncomingOrder;

public class StopOrderManager {

	private static PriceComparator priceComparator = new PriceComparator();
	private ExecutorService threadPoolExecutor;
	private TreeMap<BigDecimal, List<IncomingOrder>> orders;
	private OrderBook orderBook;
	private AtomicReference<BigDecimal> currentBestPrice;
	
	public StopOrderManager(OrderBook orderBook) {
		this.orderBook = orderBook;
		this.orders = new TreeMap<>(priceComparator.reversed());
		this.currentBestPrice = orderBook.getBestBidsLevel() != null ? new AtomicReference<BigDecimal>(orderBook.getBestBidsLevel().getPrice()) : null;
		this.threadPoolExecutor = Executors.newFixedThreadPool(5); 
	}
	
	public void addOrder(IncomingOrder order) {
		List<IncomingOrder> currentList = orders.get(order.getPrice());
		if(currentList == null) {
			currentList = new ArrayList<IncomingOrder>();
		}
		currentList.add(order);
		orders.put(order.getPrice(), currentList);
	}
	
	public void triggerOrders(BigDecimal currentMatchPrice) {
		BigDecimal lower = null;
		NavigableMap<BigDecimal, List<IncomingOrder>> toTrigger = null;
		boolean firstTime = false;
		
		if (currentBestPrice == null) {
			currentBestPrice = new AtomicReference<BigDecimal>(currentMatchPrice);
			firstTime = true;
		}
		BigDecimal oldBestPrice = currentBestPrice.getAndSet(currentMatchPrice);
		if(!orders.isEmpty() && (ArithmeticOperation.lesserThan(currentMatchPrice, oldBestPrice) || firstTime)) {
			synchronized (this) {
				
				lower = currentMatchPrice;
				BigDecimal first = orders.firstKey();
				BigDecimal last = orders.lastKey();
				if(ArithmeticOperation.lesserThan(lower, first)) {
					lower = first;
				}
				NavigableMap<BigDecimal, List<IncomingOrder>> toRemove = orders.subMap(lower, true, last, true);
				toTrigger  = new TreeMap<>(toRemove);
				toRemove.clear();
			}
			
			if(!toTrigger.isEmpty()) {
				StopOrderTrigger trigger = new StopOrderTrigger(toTrigger, orderBook);
				threadPoolExecutor.execute(trigger);
			}
		}
		
	}


}
