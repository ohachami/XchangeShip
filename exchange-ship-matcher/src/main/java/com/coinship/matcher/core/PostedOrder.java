package com.coinship.matcher.core;

import java.math.BigDecimal;

public class PostedOrder {

	private PriceLevel level;
	
	private BigDecimal stopPrice;

	private String uid;

	private BigDecimal remainingQuantity;
	
	private Integer traderId;
	
	private Integer id;

	/**
	 * @param id TODO
	 * @param level
	 * @param stopPrice TODO
	 * @param uid
	 * @param remainingQuantity
	 */
	public PostedOrder(Integer id, Integer traderId, PriceLevel level, BigDecimal stopPrice, String uid, BigDecimal remainingQuantity) {
		super();
		this.traderId = traderId;
		this.level = level;
		this.uid = uid;
		this.remainingQuantity = remainingQuantity;
	}

	public Integer getId() {
		return id;
	}

	public PriceLevel getLevel() {
		return level;
	}

	public BigDecimal getRemainingQuantity() {
		return remainingQuantity;
	}

	public void reduce(BigDecimal quantity) {
		remainingQuantity = new BigDecimal(remainingQuantity.subtract(quantity).toString());
	}

	public void resize(BigDecimal size) {
		remainingQuantity = new BigDecimal(remainingQuantity.add(size).toString());
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
	
	public BigDecimal getStopPrice() {
		return stopPrice;
	}

	/**
	 * @return the traderId
	 */
	public Integer getTraderId() {
		return traderId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostedOrder other = (PostedOrder) obj;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}

}
