package com.coinship.matcher.core;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;

import com.coinship.api.front.dto.ArithmeticOperation;
import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.api.front.dto.OrderSide;
import com.coinship.api.front.dto.ShipOrderType;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.matcher.exception.OrderBookException;
import com.coinship.matcher.strategy.IMatchingStrategy;
import com.coinship.matcher.strategy.MatchingStrategyFactory;

public class OrderBook {

    private TreeMap<BigDecimal, PriceLevel> bids;
    private TreeMap<BigDecimal, PriceLevel> asks;

    private HashMap<String, PostedOrder> orders;

    private OrderBookListener listener;
    
    private PairCurrencyBean pair;
    private StopOrderManager stopOrderManager;
    private AtomicReference<BigDecimal> lastPrice = new AtomicReference<BigDecimal>(null);

    /**
     * Create an order book.
     *
     * @param listener a listener for outbound events from the order book
     */
    public OrderBook(OrderBookListener listener, PairCurrencyBean pair) {
        this.bids = new TreeMap<>(Comparator.reverseOrder()); //OPPOSITE_COMPARATOR
        this.asks = new TreeMap<>(); //NATURAL_COMPARATOR
        this.orders = new HashMap<>();
        this.listener = listener;
        this.pair = pair;
        this.stopOrderManager = new StopOrderManager(this);
    }

    
    /**
     * Enter an order to this order book.
     *
     * <p>The incoming order is first matched against resting orders in this
     * order book. This operation results in zero or more Match events.</p>
     *
     * <p>If the remaining quantity is not zero after the matching operation,
     * the remaining quantity is added to this order book and an Add event is
     * triggered.</p>
     *
     * <p>If the order identifier is known, do nothing.</p>
     *
     * @param orderUID an order identifier
     * @param side the side
     * @param price the limit price
     * @param size the size
     */
    public void enter(IncomingOrder order) throws OrderBookException{
    	if(orders.containsKey(order.getCode())) {
    		throw new OrderBookException("The order : ".concat(order.getCode()).concat(" already exists"), pair);
    	}
    	ShipOrderType type = ShipOrderType.valueOf(order.getOrderType());
    	if(type != null) {
    		if(ShipOrderType.STOP.equals(type)) {
        		stopOrderManager.addOrder(order);
        	}else {
        		
        		if(ShipOrderType.MARKET.equals(type) && orderBookEmpty(OrderSide.BUY.equals(order.getSide()) ? asks : bids)) {
        			throw new OrderBookException("The orderbook is empty, no market order could be made", pair);
        		}
        		
        		IMatchingStrategy matchingStrategy = MatchingStrategyFactory.getInstance().getMatchingStrategy(type);
        		
    			if (OrderSide.BUY.equals(order.getSide())) {
    			    buy(order, matchingStrategy);
    			} else {
    			    sell(order, matchingStrategy);
    			}
        	}
			
    	}else {
    		//invalid order type
    		throw new OrderBookException("Invalid order type", pair);
    	}
    }

    private void buy(IncomingOrder order, IMatchingStrategy matchingStrategy) throws OrderBookException {
    	BigDecimal remainingQuantity = order.getQuantity();

        PriceLevel bestLevel = getBestAsksLevel();
        while (ArithmeticOperation.greaterThan(remainingQuantity, BigDecimal.ZERO)
        		&& bestLevel != null && matchingStrategy.canMatch(bestLevel.getPrice(), order.getPrice(), OrderSide.BUY)) {
        	MatchingDetails matchingDetails = bestLevel.match(order, OrderSide.BUY, remainingQuantity);
            remainingQuantity = matchingDetails.getRemainingQuantity();
            lastPrice.set(matchingDetails.getPrice());
            //TODO match
            if (bestLevel.isEmpty())
                asks.remove(bestLevel.getPrice());

            bestLevel = getBestAsksLevel();
        }

        if (ArithmeticOperation.greaterThan(remainingQuantity, BigDecimal.ZERO)) {
            orders.put(order.getCode(), add(bids, order, OrderSide.BUY, remainingQuantity));

            listener.add(order.getCode(), OrderSide.BUY, order.getPrice(), remainingQuantity);
        }else {
        	//order is satisfied
        	listener.complete(order.getCode());
        }
    }

    private void sell(IncomingOrder order, IMatchingStrategy matchingStrategy) throws OrderBookException {
    	BigDecimal remainingQuantity = order.getQuantity();

        PriceLevel bestLevel = getBestBidsLevel();

        while (ArithmeticOperation.greaterThan(remainingQuantity, BigDecimal.ZERO)
        		&& bestLevel != null && matchingStrategy.canMatch(bestLevel.getPrice(), order.getPrice(), OrderSide.SELL)) {
        	MatchingDetails matchingDetails = bestLevel.match(order, OrderSide.SELL, remainingQuantity);
            remainingQuantity = matchingDetails.getRemainingQuantity();
            lastPrice.set(matchingDetails.getPrice());
            if (bestLevel.isEmpty())
                bids.remove(bestLevel.getPrice());

            bestLevel = getBestBidsLevel();
        }

        if (ArithmeticOperation.greaterThan(remainingQuantity, BigDecimal.ZERO)) {
            orders.put(order.getCode(), add(asks, order, OrderSide.SELL, remainingQuantity));

            listener.add(order.getCode(), OrderSide.SELL, order.getPrice(), remainingQuantity);
        }else {
        	//order is satisfied
        	listener.complete(order.getCode());
        }
    }

    /**
     * Cancel a quantity of an order in this order book. The size refers
     * to the new order size. If the new order size is set to zero, the
     * order is deleted from this order book.
     *
     * <p>A Cancel event is triggered.</p>
     *
     * <p>If the order identifier is unknown, do nothing.</p>
     *
     * @param orderUID the order identifier
     * @param size the new size
     */
    public void cancel(String orderUID, BigDecimal size) {
        PostedOrder order = orders.get(orderUID);
        if (order == null)
            return;

        BigDecimal remainingQuantity = order.getRemainingQuantity();

        if (ArithmeticOperation.greaterOrEquals(size, remainingQuantity))
            return;

        if (ArithmeticOperation.greaterThan(size, BigDecimal.ZERO)) {
            order.resize(size);
        } else {
            delete(order);
            orders.remove(orderUID);
        }

        listener.cancel(orderUID, remainingQuantity.subtract(size), size);
    }

    private PriceLevel getBestLevel(TreeMap<BigDecimal, PriceLevel> levels) {
        if (levels.isEmpty()) {
            return null;
        }
        
        return levels.firstEntry().getValue();
    }
    
    public PriceLevel getBestBidsLevel() {
    	return getBestLevel(bids);
    }
    
    public PriceLevel getBestAsksLevel() {
    	return getBestLevel(asks);
    } 

    private PostedOrder add(TreeMap<BigDecimal, PriceLevel> levels, IncomingOrder order, OrderSide side, BigDecimal size) throws OrderBookException {
        PriceLevel level = levels.get(order.getPrice());
        if (level == null) {
        	BigDecimal price = null;
        	if(!levels.isEmpty() || order.getPrice() != null) {
        		price = order.getPrice();
        	}else {
        		price = lastPrice.get();
        	}
        	if(price == null) {
    			throw new OrderBookException("Cannot create price level with empty price", pair);
    		}
            level = new PriceLevel(side, price, this.pair, listener);
            levels.put(price, level);
        }

        return level.add(order, size);
    }
    
    private boolean orderBookEmpty(TreeMap<BigDecimal, PriceLevel> levels) {
    	return levels.isEmpty();
    }

    private void delete(PostedOrder order) {
        PriceLevel level = order.getLevel();

        level.delete(order);

        if (level.isEmpty())
            delete(level);
    }

    private void delete(PriceLevel level) {
        switch (level.getSide()) {
        case BUY:
            bids.remove(level.getPrice());
            break;
        case SELL:
            asks.remove(level.getPrice());
            break;
        }
    }

	/**
	 * @return the pair
	 */
	public PairCurrencyBean getPair() {
		return pair;
	}


	/**
	 * @return the lastPrice
	 */
	public AtomicReference<BigDecimal> getLastPrice() {
		return lastPrice;
	}

}
