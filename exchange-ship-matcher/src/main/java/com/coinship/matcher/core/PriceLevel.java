package com.coinship.matcher.core;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.coinship.api.front.dto.ArithmeticOperation;
import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.api.front.dto.OrderSide;
import com.coinship.api.persistance.dto.PairCurrencyBean;

class PriceLevel {

    private OrderSide side;

    private BigDecimal price;

    private ArrayList<PostedOrder> orders;
    
    private PairCurrencyBean pair;
    
    private OrderBookListener listener;

    public PriceLevel(OrderSide side, BigDecimal price, PairCurrencyBean pair, OrderBookListener listener) {
        this.side   = side;
        this.price  = price;
        this.pair = pair;
        this.orders = new ArrayList<>();
        this.listener = listener;
    }

    public OrderSide getSide() {
        return side;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public boolean isEmpty() {
        return orders.isEmpty();
    }

    public PostedOrder add(IncomingOrder entryOrder, BigDecimal size) {
        PostedOrder order = new PostedOrder(entryOrder.getId(), entryOrder.getTraderId(), this, null, entryOrder.getCode(), size);

        orders.add(order);

        return order;
    }

    public synchronized MatchingDetails match(IncomingOrder inOrder, OrderSide side, BigDecimal quantity) {
    	
    	MatchingDetails result = new MatchingDetails(inOrder, price, pair, side);
        while (ArithmeticOperation.greaterThan(quantity, BigDecimal.ZERO) && !orders.isEmpty()) {
            PostedOrder order = orders.get(0);

            BigDecimal orderQuantity = order.getRemainingQuantity();
            
            if (ArithmeticOperation.greaterThan(orderQuantity, quantity)) {
                order.reduce(quantity);

                result.add(new MatchingElement(order, quantity));

                quantity = BigDecimal.ZERO;
            } else {
                orders.remove(0);

                result.add(new MatchingElement(order, orderQuantity));
                
            	//resting order is satisfied
            	listener.complete(order.getUid());
            	
                quantity = quantity.subtract(orderQuantity);
            }
        }
        result.setRemainingQuantity(quantity);
        return result;
    }

    public void delete(PostedOrder order) {
        orders.remove(order);
    }

}
