package com.coinship.matcher.core;

import java.math.BigDecimal;

import com.coinship.api.front.dto.OrderSide;

/**
 * The interface for outbound events from an order book.
 */
public interface OrderBookListener {

	
    /**
     * Match an incoming order to a resting order in the order book. The match
     * occurs at the price of the order in the order book.
     * @param matchingDetails order filling information
     */
	/* post matched transactions to persistence queue, 
	 * Typically, we can create a JMS Queue that handles persistence, the project
	 * Using a EIP solution as Apache Camel could help
	 */
    void match(MatchingDetails matchingDetails);

    /**
     * Add an order to the order book.
     *
     * @param orderId the order identifier
     * @param side the side
     * @param price the limit price
     * @param size the size
     */
    /*
     * notify subscribers of orderbook's update 
     */
    void add(String orderId, OrderSide side, BigDecimal price, BigDecimal size);

    /**
     * Cancel a quantity of an order.
     *
     * @param orderId the order identifier
     * @param canceledQuantity the canceled quantity
     * @param remainingQuantity the remaining quantity
     */
    /*
     * notify subscribers of orderbook's update 
     */
    void cancel(String orderId, BigDecimal canceledQuantity, BigDecimal remainingQuantity);
    
    /**
     * 
     * @param orderId
     */
    /*
     * Set order status to 'COMPLETED' and update the database
     */
    void complete(String orderId);

}