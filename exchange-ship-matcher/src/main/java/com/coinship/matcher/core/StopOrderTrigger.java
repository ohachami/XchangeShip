package com.coinship.matcher.core;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;

import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.matcher.exception.OrderBookException;

public class StopOrderTrigger implements Runnable{

	private NavigableMap<BigDecimal, List<IncomingOrder>> stopOrders;
	private OrderBook orderBook;
	public StopOrderTrigger(NavigableMap<BigDecimal, List<IncomingOrder>> stopOrders, OrderBook orderBook) {
		this.orderBook = orderBook;
		this.stopOrders = stopOrders;
	}
	
	@Override
	public void run() {
		if(stopOrders != null && !stopOrders.isEmpty()) {
			Iterator<List<IncomingOrder>> iterator = stopOrders.values().iterator();
			while(iterator.hasNext()) {
				List<IncomingOrder> levelOrders = iterator.next();
				for(IncomingOrder toFire : levelOrders) {
					if(toFire.getLimit() != null) {
						toFire.setOrderType(OrderType.LIMIT.name());
					}else {
						toFire.setOrderType(OrderType.MARKET.name());
					}
					try {
						this.orderBook.enter(toFire);
					} catch (OrderBookException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
	}

}
