package com.coinship.matcher.core;

import java.math.BigDecimal;
import java.util.Comparator;

import com.coinship.api.front.dto.ArithmeticOperation;

public class PriceComparator implements Comparator<BigDecimal>{

	
	@Override
	public int compare(BigDecimal o1, BigDecimal o2) {
		if(o1 == null) {
			return ArithmeticOperation.LESSER;
		}
		if(o2 == null) {
			return ArithmeticOperation.GREATER;
		}
		return o1.compareTo(o2);
		
	}
	

}
