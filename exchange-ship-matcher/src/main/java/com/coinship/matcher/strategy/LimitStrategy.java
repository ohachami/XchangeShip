package com.coinship.matcher.strategy;

import java.math.BigDecimal;

import com.coinship.api.front.dto.ArithmeticOperation;
import com.coinship.api.front.dto.OrderSide;

public class LimitStrategy implements IMatchingStrategy{


	@Override
	public boolean canMatch(BigDecimal levelPrice, BigDecimal price,
			OrderSide side) {
		if(levelPrice != null && price != null && side != null) {
			return side.equals(OrderSide.BUY) ? ArithmeticOperation.lesserOrEquals(levelPrice, price) : 
				ArithmeticOperation.greaterOrEquals(levelPrice, price) ;
		}
		return false;
	}

}
