package com.coinship.matcher.strategy;

import org.springframework.stereotype.Service;

import com.coinship.api.front.dto.ShipOrderType;

@Service
public class MatchingStrategyFactory {

	private static MatchingStrategyFactory INSATNCE = new MatchingStrategyFactory();
	
	private IMatchingStrategy marketStrategy;
	private IMatchingStrategy limStrategy;
	
	private MatchingStrategyFactory() {
		marketStrategy = new MarketStrategy();
		limStrategy = new LimitStrategy();
	}
	
	public IMatchingStrategy getMatchingStrategy(ShipOrderType type) {
		if(ShipOrderType.MARKET.equals(type)) {
			return marketStrategy;
		}else if (ShipOrderType.LIMIT.equals(type)) {
			return limStrategy;
		}
		
		return null;
	}
	
	public static MatchingStrategyFactory getInstance() {
		return INSATNCE;
	}
}
