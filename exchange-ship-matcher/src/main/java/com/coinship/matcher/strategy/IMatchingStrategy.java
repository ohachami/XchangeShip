package com.coinship.matcher.strategy;

import java.math.BigDecimal;

import com.coinship.api.front.dto.OrderSide;


public interface IMatchingStrategy {

	boolean canMatch(BigDecimal levelPrice, BigDecimal price,
			OrderSide side);
}
