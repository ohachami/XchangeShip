package com.coinship.matcher.exception;

import com.coinship.api.persistance.dto.PairCurrencyBean;

public class OrderBookException extends Exception{

	private PairCurrencyBean pair;
	public OrderBookException(String message, PairCurrencyBean pair) {
		super(message);
		this.pair = pair;
	}
	/**
	 * @return the pair
	 */
	public PairCurrencyBean getPair() {
		return pair;
	}
	
}
