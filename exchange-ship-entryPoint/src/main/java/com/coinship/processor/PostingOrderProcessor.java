package com.coinship.processor;

import java.util.Calendar;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.api.front.dto.ShipOrderStatus;
import com.coinship.api.persistance.dto.OrderPostBean;
import com.coinship.api.persistance.dto.OrderStatusBean;
import com.coinship.api.persistance.dto.OrderTypeBean;
import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.persistance.dao.BasicResponse;
import com.coinship.service.IOrderService;
import com.coinship.service.ICacheService;
import com.sun.istack.NotNull;

/**
 * This processor persist the order into the DB and transform it
 * to the internal representation of the matching engine orders
 * @author Omar HACHAMI
 *
 */
public class PostingOrderProcessor implements Processor{

	private IOrderService orderService;
	
	private ICacheService cacheService;
	
	@Override
	public void process(Exchange ex) throws Exception {
		IncomingOrder body = ex.getIn(IncomingOrder.class);

		String ticket = UUID.randomUUID().toString();
		body.setCode(ticket);

		//save the order to the database
		OrderPostBean toPost = buildInitialOrder(body);
		body.setId(orderService.createOrder(toPost));
		
		ex.getIn().setBody(body);
		
		//return response  
		BasicResponse response = new BasicResponse("E00000", "Order enqueued successfully", ticket);
		ex.getOut().setBody(response);
		
	}
	
	/**
	 * map the incoming order to the matching engine representation
	 * @param order
	 * @return
	 */
	private OrderPostBean buildInitialOrder(@NotNull @Valid IncomingOrder order) {
		OrderPostBean toPost = new OrderPostBean();
		toPost.setCode(order.getCode());
		toPost.setDestAmt(order.getQuantity());
		toPost.setDestUnitPrice(order.getPrice());
		if(order.getPrice() != null) {
			toPost.setSourceAmt(order.getPrice().multiply(order.getQuantity()));
		}
		toPost.setPostingDate(Calendar.getInstance().getTime());
		
		//set order type and status
		OrderTypeBean type = (OrderTypeBean) cacheService.getByCode(OrderTypeBean.class, order.getOrderType());
		toPost.setOrderType(type);
		
		OrderStatusBean status = (OrderStatusBean) cacheService.getByCode(OrderStatusBean.class, ShipOrderStatus.POSTED.name());
		toPost.setOrderStatus(status);
		
		PairCurrencyBean pair = (PairCurrencyBean) cacheService.getByCode(PairCurrencyBean.class, order.getPair());
		 toPost.setPairCurrency(pair);
		return toPost;
	}

	/**
	 * @return the orderService
	 */
	public IOrderService getOrderService() {
		return orderService;
	}

	/**
	 * @param orderService the orderService to set
	 */
	public void setOrderService(IOrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * @return the cacheService
	 */
	public ICacheService getCacheService() {
		return cacheService;
	}

	/**
	 * @param cacheService the cacheService to set
	 */
	public void setCacheService(ICacheService cacheService) {
		this.cacheService = cacheService;
	}

}
