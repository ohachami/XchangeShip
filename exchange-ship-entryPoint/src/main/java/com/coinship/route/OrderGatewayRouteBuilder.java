package com.coinship.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;

import com.coinship.api.front.dto.IncomingOrder;
import com.coinship.persistance.dao.BasicResponse;

/**
 * A Camel route configuration class exposing a rest API to make orders.
 * @author Omar HACHAMI
 *
 */
public class OrderGatewayRouteBuilder extends RouteBuilder{

	
	@Override
	public void configure() throws Exception {

		  restConfiguration().component("servlet")
          .enableCORS(true)
          .port(9090)
          .bindingMode(RestBindingMode.json)
          .dataFormatProperty("prettyPrint", "true");
		
		  rest("/orderGateway").produces("application/json").consumes("application/json").outType(BasicResponse.class)
		  		//the call to that API triggers the "addOrder" route defined in exchange-gateway.xml
		  		.post("/makeOrder").description("post an order to the order book relative to the choosen Pair")
		  				.param().name("order").type(RestParamType.body).description("The order to make").endParam()
		  				.type(IncomingOrder.class).to("direct:addOrder")
		  		//the call to that API triggers the "addOrder" route defined in exchange-gateway.xml		  		
				.delete("/cancelOrder/{orderCode}").description("cancel an existing order by givin it's code")
						.param().name("id").type(RestParamType.path).description("The code of the order to cancel").dataType("String").endParam()
						.to("direct:cancelOrder");
	}

}
