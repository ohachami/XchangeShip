package com.coinship.route;

import org.apache.camel.Body;
import org.apache.camel.Consume;
import org.apache.camel.DynamicRouter;

import com.coinship.api.front.dto.IncomingOrder;


/**
 * Dynamic router.
 * The initial ordersQueue is split into multiple queues, one for each Orderbook.
 * The orderbook consume orders from it's own queue according to it's pair  
 * @author Omar HACHAMI
 *
 */
public class OrderBookPairRouter { 
	
	/**
	 * this method construct the route name for the given pair currency
	 * @param body
	 * @return
	 */
	@Consume(uri = "activemq:queue:ordersQueue") 
	@DynamicRouter
	public String routeAMQ(@Body IncomingOrder body) {
		if(body != null && body.getPair() != null) {
			//we create an activeMq queue for each pair, the messages will be consumed by the appropriate orderbook
			return String.format("activemq:queue:%sQueue", body.getPair());
		}
		return null;
	}
	
	
}
