package com.coinship.persistance.utils;

public class TriOrderBean {

	public String prop;
	public boolean asc;
	
	
	public TriOrderBean(String prop, boolean asc) {
		super();
		this.prop = prop;
		this.asc = asc;
	}
	
	public String getProp() {
		return prop;
	}
	public void setProp(String prop) {
		this.prop = prop;
	}
	public boolean isAsc() {
		return asc;
	}
	public void setAsc(boolean asc) {
		this.asc = asc;
	}
	
}
