package com.coinship.persistance.model;

// Generated 15 nov. 2017 16:11:59 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Currency generated by hbm2java
 */
@Entity
@Table(name = "CURRENCY", schema = "PUBLIC", catalog = "PUBLIC")
public class Currency implements java.io.Serializable {

	private int id;
	private String isoCode;
	private String name;
	private Set<PairCurrency> pairCurrenciesForSourceCur = new HashSet<PairCurrency>(
			0);
	private Set<PairCurrency> pairCurrenciesForDestCur = new HashSet<PairCurrency>(
			0);

	public Currency() {
	}

	public Currency(int id) {
		this.id = id;
	}

	public Currency(int id, String isoCode, String name,
			Set<PairCurrency> pairCurrenciesForSourceCur,
			Set<PairCurrency> pairCurrenciesForDestCur) {
		this.id = id;
		this.isoCode = isoCode;
		this.name = name;
		this.pairCurrenciesForSourceCur = pairCurrenciesForSourceCur;
		this.pairCurrenciesForDestCur = pairCurrenciesForDestCur;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "ISO_CODE", length = 3)
	public String getIsoCode() {
		return this.isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	@Column(name = "NAME", length = 256)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currencyBySourceCur")
	public Set<PairCurrency> getPairCurrenciesForSourceCur() {
		return this.pairCurrenciesForSourceCur;
	}

	public void setPairCurrenciesForSourceCur(
			Set<PairCurrency> pairCurrenciesForSourceCur) {
		this.pairCurrenciesForSourceCur = pairCurrenciesForSourceCur;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currencyByDestCur")
	public Set<PairCurrency> getPairCurrenciesForDestCur() {
		return this.pairCurrenciesForDestCur;
	}

	public void setPairCurrenciesForDestCur(
			Set<PairCurrency> pairCurrenciesForDestCur) {
		this.pairCurrenciesForDestCur = pairCurrenciesForDestCur;
	}

}
