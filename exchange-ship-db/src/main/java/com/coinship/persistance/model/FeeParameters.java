package com.coinship.persistance.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FEE_PARAMETERS")
public class FeeParameters {

	private int id;
	private BigDecimal volumeRate;
	private BigDecimal feeRate;
	/**
	 * <code>M</code> for maker<br/>
	 * <code>T</code> for taker
	 */
	private String applyTo;
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the volumeRate
	 */
	@Column(name="VOLUME_RATE")
	public BigDecimal getVolumeRate() {
		return volumeRate;
	}
	/**
	 * @param volumeRate the volumeRate to set
	 */
	public void setVolumeRate(BigDecimal volumeRate) {
		this.volumeRate = volumeRate;
	}
	/**
	 * @return the feeRate
	 */
	@Column(name="FEE_RATE")
	public BigDecimal getFeeRate() {
		return feeRate;
	}
	/**
	 * @param feeRate the feeRate to set
	 */
	public void setFeeRate(BigDecimal feeRate) {
		this.feeRate = feeRate;
	}
	/**
	 * <code>M</code> for maker<br/>
	 * <code>T</code> for taker
	 * @return the applyTo
	 */
	@Column(name="APPLY_TO")
	public String getApplyTo() {
		return applyTo;
	}
	/**
	 * @param applyTo the applyTo to set
	 */
	public void setApplyTo(String applyTo) {
		this.applyTo = applyTo;
	}
	
	
}
