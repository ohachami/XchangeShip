package com.coinship.service;

import java.util.List;

import com.coinship.api.persistance.dto.AccountBean;
import com.coinship.api.persistance.dto.TraderBean;
import com.coinship.persistance.model.Account;

public interface IAccountService  extends IBasicDAOService<Account, AccountBean>{

	void activateAccount(Integer accountId);
	
	void deactivateAccount(Integer accountId);
	
	List<AccountBean> getTraderAccounts(Integer traderId, AccountBean criteria);
	
	List<TraderBean> getTradersInfos(List<Integer> tradersIds, boolean accoutInfos);
	
	AccountBean getAccount(Integer id);
}
