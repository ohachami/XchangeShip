package com.coinship.service;

import java.math.BigDecimal;
import java.util.List;

import com.coinship.api.persistance.dto.OrderTypeBean;
import com.coinship.persistance.model.OrderType;

public interface IOrderTypeService extends IBasicDAOService<OrderType, OrderTypeBean>{

	OrderTypeBean getOrderType(BigDecimal id);
	List<OrderTypeBean> getAll();
}
