package com.coinship.service;

import java.util.List;

import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.persistance.model.PairCurrency;
import com.coinship.service.exception.BusinessException;

public interface ICurrencyPairService  extends IBasicDAOService<PairCurrency, PairCurrencyBean>{

	public List<PairCurrencyBean> getActivePairs();
	
	public List<PairCurrencyBean> getAllPairs();
	
	public void activatePair(Integer id) throws BusinessException;
	
	public void deactivatePair(Integer id) throws BusinessException;
	
}
