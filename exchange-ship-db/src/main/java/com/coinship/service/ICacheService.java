package com.coinship.service;

import java.math.BigDecimal;
import java.util.List;

import com.coinship.api.persistance.dto.BasicDto;

/**
 * Interface providing cache mechanism to avoid multiple access
 * to the database to retrieve immutable parameters
 * @author Omar HACHAMI
 *
 */
public interface ICacheService {
	
	/**
	 * Get a record by it's id (primary key)
	 * @param key
	 * @param id
	 * @return
	 */
	BasicDto getById(Class<?> key, BigDecimal id);
	
	/**
	 * Get a record by it's code (functional unique id)
	 * @param key
	 * @param code
	 * @return
	 */
	BasicDto getByCode(Class<?> key, String code);
	
	/**
	 * Get all values (records) of a specific class (table)
	 * @param key
	 * @return
	 */
	List<? extends BasicDto> getAll(Class<?> key);
}
