package com.coinship.service;

import java.math.BigDecimal;
import java.util.List;

import com.coinship.api.persistance.dto.OrderStatusBean;
import com.coinship.persistance.model.OrderStatus;

public interface IOrderStatusService  extends IBasicDAOService<OrderStatus, OrderStatusBean>{

	OrderStatusBean getStatus(BigDecimal id);
	List<OrderStatusBean> getAll();
}
