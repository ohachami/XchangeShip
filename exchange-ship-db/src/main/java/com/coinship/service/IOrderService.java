package com.coinship.service;

import com.coinship.api.persistance.dto.OrderPostBean;
import com.coinship.persistance.model.OrderPost;

public interface IOrderService extends IBasicDAOService<OrderPost, OrderPostBean>{

	Integer createOrder(OrderPostBean order);
	void updateOrder(OrderPostBean order);
	void cancelOrder(Integer id);
	OrderPostBean getOrder(Integer id);
}
