package com.coinship.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.coinship.persistance.dao.AbstractDAO;
import com.coinship.persistance.utils.GalopiaCriterion;
import com.coinship.persistance.utils.TriOrderBean;

public interface IBasicDAOService<T,U> {

	public U saveOrUpdate(U frontBean);
	
	public void delete(Integer id);
	
	public List<U> search(Criterion root, Integer firstElmt, Integer maxResult, List<TriOrderBean> orders, GalopiaCriterion ...associations);
	
	
	public U findById(Serializable id);
	
	public List<U> findAll();
	
	public AbstractDAO<T> getDao();
}
