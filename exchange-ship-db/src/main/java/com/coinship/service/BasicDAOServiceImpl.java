package com.coinship.service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.coinship.persistance.dao.BasicDAO;
import com.coinship.persistance.utils.ExampleBuilder;
import com.coinship.persistance.utils.GalopiaCriterion;
import com.coinship.persistance.utils.TriOrderBean;
import com.coinship.service.mapper.CollectionMapperDecorator;

@Transactional
public class BasicDAOServiceImpl<T,U> implements IBasicDAOService<T, U> {

	protected BasicDAO<T> dao;
	@Autowired
	protected CollectionMapperDecorator collectionMapper;
	private SessionFactory sessionFactory;
	
	private Class<U> frontClass;
	
	public BasicDAOServiceImpl() {
		this.frontClass = ((Class<U>) ((ParameterizedType) getClass()
		        .getGenericSuperclass()).getActualTypeArguments()[1]);
	}
	
	public U saveOrUpdate(U frontBean) {
		T entity = collectionMapper.map(frontBean, dao.getClazz());
		return collectionMapper.map(dao.merge(entity), frontClass);
	}
	
	public void delete(Integer id) {
		dao.deleteById(id);
	}
	
	public List<U> search(Criterion root, Integer firstElmt, Integer maxResult, List<TriOrderBean> orders, GalopiaCriterion ...associations) {
		
		if(root != null) {
			List<T> result = dao.getListByExemple(firstElmt, maxResult, orders, 
					ExampleBuilder.buildCriteria(dao.getClazz(), root, associations));
			if(result != null) {
				return collectionMapper.mapCollection(result, frontClass);
			}
		}
		return null;
	}

	@Override
	public U findById(Serializable id) {
		T result = dao.getById(id);
		return result != null ? collectionMapper.map(result, frontClass) : null;
	}

	public CollectionMapperDecorator getCollectionMapper() {
		return collectionMapper;
	}

	public void setCollectionMapper(CollectionMapperDecorator collectionMapper) {
		this.collectionMapper = collectionMapper;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public BasicDAO<T> getDao() {
		return dao;
	}

	@Autowired
	public void setDao(BasicDAO<T> dao) {
		this.dao = dao;
		this.dao.setClazz(((Class<T>) ((ParameterizedType) getClass()
		        .getGenericSuperclass()).getActualTypeArguments()[0]));
	}

	@Override
	public List<U> findAll() {
		List<T> results = dao.findAll();
		return results != null ? collectionMapper.mapCollection(results, frontClass) : null;
	}	
	
}
