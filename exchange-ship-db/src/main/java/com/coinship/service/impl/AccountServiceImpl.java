package com.coinship.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coinship.api.persistance.dto.AccountBean;
import com.coinship.api.persistance.dto.TraderBean;
import com.coinship.persistance.model.Account;
import com.coinship.service.BasicDAOServiceImpl;
import com.coinship.service.IAccountService;

@Service
@Transactional
public class AccountServiceImpl extends BasicDAOServiceImpl<Account, AccountBean> implements IAccountService {

	@Override
	public void activateAccount(Integer accountId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deactivateAccount(Integer accountId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<AccountBean> getTraderAccounts(Integer traderId,
			AccountBean criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TraderBean> getTradersInfos(List<Integer> tradersIds,
			boolean accoutInfos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccountBean getAccount(Integer id) {
		return id != null ? collectionMapper.map(findById(id), AccountBean.class) : null;
	}

}
