package com.coinship.service.impl;

import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coinship.api.persistance.dto.PairCurrencyBean;
import com.coinship.persistance.model.PairCurrency;
import com.coinship.persistance.utils.ExampleBuilder;
import com.coinship.persistance.utils.GalopiaCriterion;
import com.coinship.service.BasicDAOServiceImpl;
import com.coinship.service.ICurrencyPairService;
import com.coinship.service.exception.BusinessException;

@Service("currencyPairService")
@Transactional
public class CurrencyPairServiceImpl extends BasicDAOServiceImpl<PairCurrency, PairCurrencyBean> implements ICurrencyPairService{

	public List<PairCurrencyBean> getActivePairs() {
		PairCurrency active = new PairCurrency();
		active.setActive(true);
		return search(ExampleBuilder.buildExample(active), null, null, null, (GalopiaCriterion[])null);
	}
	
	public List<PairCurrencyBean> getAllPairs() {
		return findAll();
	}
	
	public void activatePair(Integer id) throws BusinessException{
		setStatus(id, true);
	}
	
	public void deactivatePair(Integer id) throws BusinessException{
		setStatus(id, false);
	}
	
	private void setStatus(Integer id, boolean status)throws BusinessException {
		PairCurrencyBean toChange = findById(id);
		if(toChange != null) {
			if(BooleanUtils.isTrue(toChange.getActive()) != status) {
				toChange.setActive(status);
				toChange = saveOrUpdate(toChange);
			}else {
				//TODO already has this status
				String errorCode = status ? "" : "";
				throw new BusinessException("", errorCode);
			}
		}else {
			//TODO
			throw new BusinessException("", "");
		}
	}
	
}
