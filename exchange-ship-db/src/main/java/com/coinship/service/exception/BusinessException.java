package com.coinship.service.exception;

public class BusinessException extends Exception{

	private String errorCode;
	
	public BusinessException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
}
